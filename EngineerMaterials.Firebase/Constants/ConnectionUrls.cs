﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EngineerMaterials.Firebase.Constants
{
    public static class ConnectionUrls
    {
        public static string RegisterUrl(string apiKey)
        {
            return $"https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key={apiKey}";
        }

        public static string MailVerification(string apiKey)
        {
            return $"https://www.googleapis.com/identitytoolkit/v3/relyingparty/getOobConfirmationCode?key={apiKey}";
        }

        public static string SignInUrl(string apiKey)
        {
            return $"https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key={apiKey}";
        }

        public static string RefreshToken(string apiKey)
        {
            return $"https://securetoken.googleapis.com/v1/token?key={apiKey}";
        }

        public static string UserData(string apiKey)
        {
            return $"https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo?key={apiKey}";
        }

        public static string DbBaseUrl(string projectId)
        {
            return $"https://{projectId}.firebaseio.com/";
        }

        public static string EqualTo(this string url, string[] compareProp, string value)
        {
            var sym = url.Contains("?") ? "&" : "?";
            return $"{url}{sym}orderBy=\"{string.Join('/', compareProp)}\"&equalTo=\"{value}\"";
        }
    }
}
