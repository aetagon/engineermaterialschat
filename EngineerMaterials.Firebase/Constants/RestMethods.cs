﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EngineerMaterials.Firebase.Constants
{
    internal class RestMethods
    {
        internal static async Task<Res> Get<Res>(string uri)
        {
            using (var client = new HttpClient())
            {
                var res = await client.GetAsync(uri);

                using (var content = res.Content)
                {
                    string result = await content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Res>(result);
                }
            }
        }

        internal static async Task<Res> Post<Req, Res>(string uri, Req req)
        {
            using (var client = new HttpClient())
            {
                var jsonInString = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                var res = await client.PostAsync(uri, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

                using (var content = res.Content)
                {
                    string result = await content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Res>(result);
                }
            }
        }

        internal static async Task<Res> Put<Req, Res>(string uri, Req req)
        {
            using (var client = new HttpClient())
            {
                var jsonInString = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                var res = await client.PutAsync(uri, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

                using (var content = res.Content)
                {
                    string result = await content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Res>(result);
                }
            }
        }
    }
}
