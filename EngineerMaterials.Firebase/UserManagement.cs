﻿using EngineerMaterials.Firebase.Constants;
using System.Threading.Tasks;
using EngineerMaterials.Firebase.Types;

namespace EngineerMaterials.Firebase
{
    public class UserManagement
    {
        string ApiKey;

        public UserManagement(string apiKey)
        {
            ApiKey = apiKey;
        }

        public async Task<FirebaseUserResponse> Register(string email, string password)
        {
            var uri = ConnectionUrls.RegisterUrl(ApiKey);
            var req = new FirebaseUserRequest
            {
                email = email,
                password = password,
                returnSecureToken = true
            };
            return await RestMethods.Post<FirebaseUserRequest, FirebaseUserResponse>(uri, req);
        }

        public async Task<VerificationMailResponse> SendVerificationMail(string idToken)
        {
            var uri = ConnectionUrls.MailVerification(ApiKey);
            var req = new VerificationMailRequest
            {
                idToken = idToken,
                requestType = "VERIFY_EMAIL"
            };

            return await RestMethods.Post<VerificationMailRequest, VerificationMailResponse>(uri, req);
        }

        public async Task<FirebaseUserResponse> SignIn(string email, string password)
        {
            var uri = ConnectionUrls.SignInUrl(ApiKey);
            var req = new FirebaseUserRequest
            {
                email = email,
                password = password,
                returnSecureToken = true
            };
            return await RestMethods.Post<FirebaseUserRequest, FirebaseUserResponse>(uri, req);
        }

        public async Task<RefreshTokenResponse> RefreshToken(string refreshToken)
        {
            var uri = ConnectionUrls.SignInUrl(ApiKey);
            var req = new RefreshTokenRequest
            {
                grant_type = "refresh_token",
                refresh_token = refreshToken
            };
            return await RestMethods.Post<RefreshTokenRequest, RefreshTokenResponse>(uri, req);
        }

        public async Task<UserDataResponse> GetMail(string idToken)
        {
            var uri = ConnectionUrls.UserData(ApiKey);
            var req = new UserDataRequest
            {
                idToken = idToken
            };
            return await RestMethods.Post<UserDataRequest, UserDataResponse>(uri, req);
        }
    }
}
