﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineerMaterials.Firebase.Types
{
    public class VerificationMailRequest
    {
        public string requestType { get; set; }
        public string idToken { get; set; }
    }

    public class VerificationMailResponse : AuthResponse
    {
        public string kind { get; set; }//string The request type, always "identitytoolkit#GetOobConfirmationCodeResponse".
        public string email { get; set; }//string The email of the account.
    }
}
