﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineerMaterials.Firebase.Types
{
    public class UserDataRequest
    {
        public string idToken { get; set; }
    }

    public class UserDataResponse : AuthResponse
    {
        public List<UserData> users { get; set; }
    }

    public class UserData {
        public string localId { get; set; } //string The uid of the current user.
        public string email { get; set; } //string The email of the account.
        public bool? emailVerified { get; set; } //boolean Whether or not the account's email has been verified.
        public string displayName { get; set; } //string The display name for the account.
        public List<dynamic> providerUserInfo { get; set; } //List of JSON objects List of all linked provider objects which contain "providerId" and "federatedId".
        public string photoUrl { get; set; } //string The photo Url for the account.
        public string passwordHash { get; set; } //string Hash version of password.
        public double? passwordUpdatedAt { get; set; } //double The timestamp, in milliseconds, that the account password was last changed.
        public string validSince { get; set; } //string The timestamp, in seconds, for the given Firebase ID token.
        public bool? disabled { get; set; } //boolean Whether the account is disabled or not.
        public string lastLoginAt { get; set; } //string The timestamp, in milliseconds, that the account last logged in at.
        public string createdAt { get; set; } //string The timestamp, in milliseconds, that the account was created at.
        public bool? customAuth { get; set; } //boolean Whether the account is authenticated by the developer.
    }
}
