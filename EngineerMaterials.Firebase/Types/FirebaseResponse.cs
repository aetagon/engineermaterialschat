﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineerMaterials.Firebase.Types
{
    public class AuthResponse
    {
        public Error error { get; set; }
        public Error fullError { get { return error; } }
        public string message { get { return error?.message; } }
    }

    public class Error
    {
        public Error()
        {

        }

        public Error(string[] messages)
        {
            if (messages == null)
                return;
            var _errors = new List<ErrorDetails>();
            foreach (var message in messages)
                _errors.Add(new ErrorDetails
                {
                    message = message
                });

            errors = _errors.ToArray();
        }

        public ErrorDetails[] errors { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class ErrorDetails
    {
        public string domain { get; set; }
        public string reason { get; set; }
        public string message { get; set; }
    }

    public class DbResponse
    {
        public Error fullError { get; set; }
        public string error { get; set; }
        public int? code { get; set; }
        public string message { get; set; }
    }
}
