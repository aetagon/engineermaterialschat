﻿
namespace EngineerMaterials.Firebase.Types
{
    public class FirebaseUserRequest
    {
        public string email { get; set; }
        public string password { get; set; }
        public bool returnSecureToken { get; set; }
    }

    public class FirebaseUserResponse : AuthResponse
    {
        public string kind { get; set; }//string The request type, always "identitytoolkit#VerifyPasswordResponse".
        public string idToken { get; set; }// string A Firebase Auth ID token for the authenticated user.
        public string email { get; set; }//string The email for the authenticated user.
        public string refreshToken { get; set; }//string A Firebase Auth refresh token for the authenticated user.
        public string expiresIn { get; set; }//string The number of seconds in which the ID token expires.
        public string localId { get; set; }//string The uid of the authenticated user.
        public bool registered { get; set; }// boolean Whether the email is for an existing account.
    }
}
