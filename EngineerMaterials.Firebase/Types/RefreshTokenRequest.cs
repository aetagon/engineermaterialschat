﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineerMaterials.Firebase.Types
{
    public class RefreshTokenRequest
    {
        public string grant_type { get; set; }//string The refresh token's grant type, always "refresh_token".
        public string refresh_token { get; set; }//string A Firebase Auth refresh token.
    }

    public class RefreshTokenResponse : AuthResponse
    {
        public string expires_in { get; set; }// string The number of seconds in which the ID token expires.
        public string token_type { get; set; }//string The type of the refresh token, always "Bearer".
        public string refresh_token { get; set; }//string The Firebase Auth refresh token provided in the request or a new refresh token.
        public string id_token { get; set; }//string A Firebase Auth ID token.
        public string user_id { get; set; }//string The uid corresponding to the provided ID token.
        public string project_id { get; set; }//string Your Firebase project ID.
    }
}
