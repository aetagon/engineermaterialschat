﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineerMaterials.Firebase.Types
{
    public class AddObjectResponse : DbResponse
    {
        public string name { get; set; }
    }
}
