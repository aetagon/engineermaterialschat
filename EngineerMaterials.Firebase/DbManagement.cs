﻿using EngineerMaterials.Firebase.Constants;
using EngineerMaterials.Firebase.Types;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EngineerMaterials.Firebase
{
    public class DbManagement
    {
        readonly string _projectId;

        public DbManagement(string projectId)
        {
            _projectId = projectId;
        }

        public async Task<Res> GetObject<Res>(string[] pathProps, string idToken = null)
        {
            var baseUrl = ConnectionUrls.DbBaseUrl(_projectId);
            var url = $"{baseUrl}{string.Join('/', pathProps)}.json?auth={idToken}";
            var res = await RestMethods.Get<Res>(url);

            return res;
        }

        public async Task<List<Res>> GetObjects<Res>(string[] pathProps, Tuple<string[], string> equalTo, string idToken = null)
        {
            var baseUrl = ConnectionUrls.DbBaseUrl(_projectId);
            var url = $"{baseUrl}{string.Join('/', pathProps)}.json?auth={idToken}";
            if (equalTo != null)
                url = url.EqualTo(equalTo.Item1, equalTo.Item2);
            var res = await RestMethods.Get<dynamic>(url);

            var result = new List<Res>();
            foreach (var item in res)
            {
                var formatedItem = Newtonsoft.Json.JsonConvert.DeserializeObject<Res>(item.Value.ToString());
                result.Add(formatedItem);
            }

            return result;
        }

        public async Task<AddObjectResponse> AddObject<T>(string[] pathProps, T request, string idToken = null)
        {
            var baseUrl = ConnectionUrls.DbBaseUrl(_projectId);
            var url = $"{baseUrl}{string.Join('/', pathProps)}.json?auth={idToken}";
            var res = await RestMethods.Post<T, AddObjectResponse>(url, request);

            return res;
        }

        public async Task<T> UpdateObject<T>(string[] pathProps, T request, string idToken = null)
        {
            var baseUrl = ConnectionUrls.DbBaseUrl(_projectId);
            var url = $"{baseUrl}{string.Join('/', pathProps)}.json?auth={idToken}";
            var res = await RestMethods.Put<T, T>(url, request);

            return res;
        }
    }
}
