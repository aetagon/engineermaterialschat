﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EngineerMaterials.OAuth.Configuration
{
    public class InMemoryConfiguration
    {
        public static IEnumerable<ApiResource> ApiResources()
        {
            return new[]{
                new ApiResource("engineermaterialschat", "Egineer Materials Chat App")
            };
        }

        public static IEnumerable<IdentityResource> IdentityResources()
        {
            return new IdentityResource[]{
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        public static IEnumerable<Client> Clients()
        {
            return new[] {
                new Client{
                    ClientId  = "engineermaterialschat",
                    ClientSecrets = new [] { new Secret("secret".Sha256())},
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    AllowedScopes = new []{ "engineermaterialschat" }
                },
                new Client{
                    ClientId  = "engineermaterials_implicit",
                    ClientSecrets = new [] { new Secret("secret".Sha256())},
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = new []{
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "engineermaterialschat"
                    },
                    RedirectUris = new []{ "http://localhost:54741/signin-oidc" },
                    PostLogoutRedirectUris = new [] { "http://localhost:54741/signin-oidc/signout-callback-oidc" }
                }
            };
        }

        public static IEnumerable<TestUser> Users()
        {
            return new[] {
                new TestUser{
                    SubjectId = "1",
                    Username="sotiriskakavoulis@gmail.com",
                    Password="password"
                }
            };
        }
    }
}
