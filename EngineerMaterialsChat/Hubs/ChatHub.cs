﻿using System;
using System.Threading.Tasks;
using EngineerMaterials.Chat.Engine;
using Microsoft.AspNetCore.SignalR;


namespace EngineerMaterials.Chat
{
    public class ChatHub : Hub
    {
        Ishi _ishi = new Ishi();

        public ChatHub()
        {
            _ishi.OnNewMessageCreated += _ishi_OnNewMessageCreated;
        }

        private void _ishi_OnNewMessageCreated(Message newMessage)
        {
            Clients.Group(newMessage.ChatRoomName)
                         .InvokeAsync("MessageToClient", newMessage);
        }

        public async Task SendToClients(Message message)
        {
            await CreateChatRoom(message);

            message.Timestamp = DateTime.Now;
            _ishi.Input(message.ChatRoomName, message.Text.Split(' '));

            await Clients.Group(message.ChatRoomName)
                         .InvokeAsync("MessageToClient", message);
        }

        private async Task CreateChatRoom(Message message)
        {
            await Groups.AddAsync(Context.ConnectionId, message.ChatRoomName);
        }
    }

    public class Message
    {
        public string Text { get; set; }
        public string Sender { get; set; }
        public DateTime Timestamp { get; set; }
        public string ChatRoomName { get; set; }
        public string AttachmentUrl { get; set; }
    }
}