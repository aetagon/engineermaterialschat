using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using EngineerMaterials.Chat.Services;
using System.ComponentModel.DataAnnotations;
using EngineerMaterials.Chat.Constants;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using EngineerMaterials.Firebase.Types;
using EngineerMaterials.Firebase;
using EngineerMaterials.Mailgun;

namespace EngineerMaterials.Chat.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ILogger<RegisterModel> _logger;
        private readonly IConfiguration _config;
        private readonly UserManagement _fbManager;
        private readonly DbManagement _dbManager;
        public ProjectController(
            ILogger<RegisterModel> logger,
            IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _fbManager = new UserManagement(_config["Firebase:ApiKey"]);
            _dbManager = new DbManagement(_config["Firebase:ProjectId"]);
        }

       

        #region private methods       

        #endregion
    }    
}
