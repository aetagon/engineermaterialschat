using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;
using EngineerMaterials.Chat.Constants;
using Microsoft.Extensions.Configuration;
using EngineerMaterials.Firebase.Types;
using EngineerMaterials.Firebase;
using EngineerMaterials.Mailgun;
using EngineerMaterials.Chat.Implementation;
using System.Runtime.Serialization;

namespace EngineerMaterials.Chat.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;
        private readonly UserManagement _fbManager;
        private readonly DbManagement _dbManager;
        //FullImplementation _service;

        public HomeController(IConfiguration config)
        {
            _config = config;
            _fbManager = new UserManagement(_config["Firebase:ApiKey"]);
            _dbManager = new DbManagement(_config["Firebase:ProjectId"]);
            //_service = new FullImplementation(_config, HttpContext, ModelState);
        }

        //[Authorize]
        public IActionResult Index()
        {
            var claims = User.Claims;
            return View();
        }

        public IActionResult Error()
        {
            //ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        [HttpPost]
        public Response<FirebaseUserResponse> Register([FromBody]RegisterModel Input)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<FirebaseUserResponse> action = () => service.Register(Input);
            return HandleAction(action);
        }

        [HttpPost]
        public Response<FirebaseUserResponse> SignIn([FromBody]LoginModel Input)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<FirebaseUserResponse> action = () => service.SignIn(Input);
            return HandleAction(action);
        }

        [HttpGet]
        public Response<ApplicationUser> GetUser(string id)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<ApplicationUser> action = () => service.GetUser(id);
            return HandleAction(action);
        }

        [HttpPut]
        public Response<ApplicationUser> UpdateUser([FromBody] ApplicationUser user)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<ApplicationUser> action = () => service.UpdateUser(user);
            return HandleAction(action);
        }

        [HttpPost]
        public Response<AddObjectResponse> AddProject([FromBody] Project project)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<AddObjectResponse> action = () => service.AddProject(project);
            return HandleAction(action);
        }

        [HttpPut]
        public Response<Project> UpdateProject([FromBody] Project project)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<Project> action = () => service.UpdateProject(project);
            return HandleAction(action);
        }

        [HttpGet]
        public Response<Project> GetProject(string id)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<Project> action = () => service.GetProject(id);
            return HandleAction(action);
        }

        [HttpGet]
        public Response<Projects> GetProjects(string id)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<Projects> action = () => service.GetProjects(id);
            return HandleAction(action);
        }

        [HttpGet]
        public Response<SharedUsers> GetSharedUsers(string id)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<SharedUsers> action = () => service.GetSharedUsers(id);
            return HandleAction(action);
        }

        [HttpPost]
        public Response<AddObjectResponse> AddProjectUser([FromBody]User2ProjectRequest u2p)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<AddObjectResponse> action = () => service.AddProjectUser(u2p);
            return HandleAction(action);
        }

        [HttpGet]
        public Response<Profiles> GetProfiles()
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<Profiles> action = () => service.GetProfiles();
            return HandleAction(action);
        }

        [HttpGet]
        public Response<DbResponse> GetFiles(string id)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<DbResponse> action = () => service.GetFiles(id);
            return HandleAction(action);
        }

        [HttpPost]
        public Response<DbResponse> AddFile([FromBody] FireBaseFile file)
        {
            var service = new FullImplementation(_config, HttpContext, ModelState);
            Func<DbResponse> action = () => service.AddFile(file);
            return HandleAction(action);
        }

        #region private methods

        //private async Task<List<User2Project>> getUser2Projet(string id, Microsoft.Extensions.Primitives.StringValues idToken, string[] objectPath, string[] propPath)
        //{
        //    var equalProp = new Tuple<string[], string>(propPath, id);
        //    var u2ps = await _dbManager.GetObjects<User2Project>(objectPath, equalProp, idToken);
        //    return u2ps;
        //}

        //private async Task<List<FireBaseFile>> getFiles(string id, Microsoft.Extensions.Primitives.StringValues idToken, string[] objectPath, string[] propPath)
        //{
        //    var equalProp = new Tuple<string[], string>(propPath, id);
        //    var files = await _dbManager.GetObjects<FireBaseFile>(objectPath, equalProp, idToken);
        //    return files;
        //}

        //private static T createError<T>(string[] messages) where T : class
        //{
        //    return new FirebaseUserResponse
        //    {
        //        error = new Firebase.Types.Error(messages)
        //    } as T;
        //}

        //private static string composeMessage(string mail, string password, string url, string message)
        //{
        //    return $"<h1>Engineer Materials Invitation</h1>" +
        //        $"<p>You have been invited to join the engineermaterials application. " +
        //        $"Use the credentials username:{mail}, password:{password} to login here <a href='{url}'>{url}</a></p>" +
        //        $"<p><strong>Invitation message: </string> {message}</p>";
        //}

        //private ApplicationUser getUserFromDb(string id, string idToken)
        //{
        //    string[] objectPath = { "users", id };
        //    var res = _dbManager.GetObject<ApplicationUser>(objectPath, idToken);

        //    return res.Result;
        //}

        //private ApplicationUser addUserToDb(ApplicationUser user, string idToken)
        //{
        //    string[] objectPath = { "users", user.id };
        //    var res = _dbManager.UpdateObject(objectPath, user, idToken);

        //    return res.Result;
        //}

        //private string getEmail(string idToken)
        //{
        //    var res = _fbManager.GetMail(idToken);
        //    var taskRes = res.Result;
        //    var user = taskRes.users.FirstOrDefault();
        //    return user?.email;
        //}

        //private string getIdFromMail(string email, string idToken)
        //{
        //    string[] objectPath = { "users" };
        //    var equalTo = new Tuple<string[], string>(new string[] { "email" }, email);
        //    var res = _dbManager.GetObjects<ApplicationUser>(objectPath, equalTo, idToken);
        //    var taskRes = res.Result;
        //    var user = taskRes.FirstOrDefault();
        //    return user?.id;
        //}

        //private List<Profile> getProfilesFromDb(string idToken)
        //{
        //    string[] objectPath = { "profiles" };
        //    var res = _dbManager.GetObject<List<Profile>>(objectPath, idToken);

        //    return res.Result;
        //}

        //private Dictionary<string, string> getProfiles(string idToken)
        //{
        //    if (_profiles == null)
        //    {
        //        _profiles = new Dictionary<string, string>();
        //        var objectPath = new string[] { "profiles" };
        //        var task = _dbManager.GetObjects<Profile>(objectPath, null, idToken);
        //        foreach (var profile in task.Result)
        //            _profiles.Add(profile.id, profile.name);
        //    }

        //    return _profiles;
        //}

        #endregion

        public Response<T> HandleAction<T>(Func<T> action)
        {
            try
            {
                return new Response<T>
                {
                    Payload = action()
                };
            }
            catch (Exception ex)
            {
                return new Response<T>
                {
                    FullError = new Firebase.Types.Error
                    {
                        message = ex.Message
                    }
                };
            }
        }
    }

    public class RegisterModel
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }

        [DataType(DataType.Password)]
        public string password { get; set; }

        public string invitationMessage { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
    }

    public class ApplicationUser : AuthResponse
    {
        public string id { get; set; }
        public string email { get; set; }
        public string surname { get; set; }
        public string name { get; set; }
        public string occupation { get; set; }
        public string profilePicUrl { get; set; }
        public string pagePicUrl { get; set; }
        public string registered { get; set; }
    }

    public class Project : DbResponse
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string creation { get; set; }
        public string creator { get; set; }
        public string creatorName { get; set; }
        public string role { get; set; }
    }

    public class Projects : DbResponse
    {
        public List<Project> projects { get; set; }
    }

    public class Profile : DbResponse
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Profiles : DbResponse
    {
        public List<Profile> profiles { get; set; }
    }

    public class User2ProjectRequest
    {
        public string projectId { get; set; }
        public string userEmail { get; set; }
        public string profileId { get; set; }
    }

    public class User2Project : DbResponse
    {
        public string projectId { get; set; }
        public string userId { get; set; }
        public string profileId { get; set; }
    }

    public class SharedUser
    {
        public string name { get; set; }
        public string email { get; set; }
        public string role { get; set; }
    }

    public class SharedUsers : DbResponse
    {
        public List<SharedUser> users { get; set; }
    }

    public class FireBaseFile : DbResponse
    {
        public string projectId { get; set; }
        public string fileName { get; set; }
        public string fileContent { get; set; }
        public string dateCreated { get; set; }
        public string dateUpdated { get; set; }
    }

    public class FireBaseFiles : DbResponse
    {
        public List<FireBaseFile> files { get; set; }
    }

    public class Response<T>
    {
        [DataMember(Name = "payload")]
        public T Payload { get; set; }
        [DataMember(Name = "fullError")]
        public Error FullError { get; set; }
    }
}
