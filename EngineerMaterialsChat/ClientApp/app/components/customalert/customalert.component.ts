import { Component, Input, Output } from '@angular/core';
import { AlertService, Alert } from '../../services/alert.service';

@Component({
  selector: 'custom-alert',
  templateUrl: './customalert.component.html',
  styleUrls: ['./customalert.component.css'],
})
export class CustomAlertComponent {

  alerts: Array<Alert> = [];

  constructor(private alert: AlertService) {
    alert.alertdAdded.subscribe(
      (a: Alert) => {
        this.alerts.push(a);
      }
    );
  }

  removeAlert(alert: Alert) {
    let index = this.alerts.indexOf(alert);
    if (index > -1)
      this.alerts.splice(index, 1);
  }
}

