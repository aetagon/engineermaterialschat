import { Component, AfterViewInit, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformServer, isPlatformBrowser } from '@angular/common';
import { AlertService } from '../../services/alert.service';
import { UserManagementService, AppUser } from '../../services/userManagement.service';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent {
  user: any = {};
  updateUser: AppUser = new AppUser(null);
  editable: boolean = false;

  constructor(private userManager: UserManagementService, private alerts: AlertService, public projectFunc: ProjectsService) {
    this.user = this.userManager.getCurrentUser();
    this.updateUser = Object.assign({}, this.user);
  }

  ngAfterViewInit() {

  }

  private toggleEdit() {
    this.editable = !this.editable;
  }

  public updateUserInfo() {
    const user = this.updateUser;
    if (user)
      this.userManager.update(user).then(
        (resp) => {
          this.alerts.addSuccess("Update success", "User was successfuly updated");

          this.toggleEdit();
          let promise = this.userManager.getUserInfo();
          if (promise)
            promise.then(
              resp => {
                this.user = this.userManager.getCurrentUser();
                this.updateUser = Object.assign({}, this.user);
              },
              error => { });
        },
        (error) => {
          this.alerts.addSuccess("Update error", error);
        });
  }

  public showImage() {
    var preview: any = document.getElementById('modal-image'); //selects the query named img
    var input: any = document.getElementById("file-input")
    var file = input.files[0]; //sames as here
    var reader = new FileReader();

    reader.onloadend = function () {
      preview.src = reader.result;
    }

    if (file) {
      reader.readAsDataURL(file); //reads the data as a URL
    } else {
      preview.src = "";
    }
  }

  invitationMail: string;
  invitationMessage: string;
  public sendInvitation() {
    let mail = this.invitationMail;
    let invitationMsg = this.invitationMessage;

    this.userManager.register(mail, invitationMsg).then(
      (resp) => {
        this.alerts.addSuccess('Invitation success', "Your invitation was successfuly sent");
      },
      (error) => {
        for (let err of error.errors)
          this.alerts.addError("Login Error", err.message);
      });
  }

}