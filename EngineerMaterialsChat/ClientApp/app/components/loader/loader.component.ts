import { Component } from '@angular/core';
import { CustomHttpService } from '../../services/customHttp.service';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent {
  constructor(private http: CustomHttpService) {

  }

  showLoader() {
    return this.http.hasPendingConnections();
  }

} 