import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'custom-modal',
  templateUrl: './custommodal.component.html',
  styleUrls: ['./custommodal.component.css'],
})
export class CustomModalComponent {
  @Input() modelId: string = "modal-default";
  @Input() header: string = 'this is header';
  @Input() footer: string = 'this is footer';
}