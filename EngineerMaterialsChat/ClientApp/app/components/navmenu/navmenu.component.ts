import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserManagementService, AppUser } from '../../services/userManagement.service';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'nav-menu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css'],
})
export class NavMenuComponent {
  userFullName: string;
  user: any = {};

  constructor(private userManager: UserManagementService, private router: Router, public projectFunc: ProjectsService) {
    this.user = this.userManager.getCurrentUser();
  }

  ngAfterViewInit() {

  }

  getFullName() {
    if (this.user)
      return `${this.user.name} ${this.user.surname}`;
    return "";
  }

  signOut() {
    this.userManager.signOut();
  }


}
