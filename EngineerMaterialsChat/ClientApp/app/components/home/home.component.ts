import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustomHttpService } from '../../services/customHttp.service';
import { ProjectsService } from '../../services/projects.service';
import { AlertService } from '../../services/alert.service';
import { Project } from '../../classes/Project';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(
    private http: CustomHttpService,
    private projectFunc: ProjectsService,
    private alerts: AlertService,
    private router: Router) {

    let promise = projectFunc.findAll();
    if (promise)
      promise.then(
        (resp: any) => {
          this.projects = resp.projects.map((x: any) => new Project(x));
        },
        (error: any) => {
          alerts.addError("Projects error", error);
        });
  }

  projects: Array<Project> = [];

  months: IDictionaryNumberKey = {
    0: "Jan",
    1: "Feb",
    2: "Mar",
    3: "Apr",
    4: "May",
    5: "Jun",
    6: "Jul",
    7: "Aug",
    8: "Sep",
    9: "Oct",
    10: "Nov",
    11: "Dec",
  }

  roleClassNames: IDictionaryStringKey = {
    "owner": "label-success",
    "viewer": "label-warning",
  }

  //dateToString(date: Date) {
  //  if (!date)
  //    return null;
  //  let res = "";

  //  res += date.getDate() + " ";
  //  res += this.months[date.getMonth()] + " ";
  //  res += date.getFullYear() + " ";
  //  res += date.getHours() + ":";
  //  res += date.getMinutes() + ":";
  //  res += date.getSeconds();

  //  return res;
  //}

  newProject() {
    let promise = this.projectFunc.add();
    if (promise)
      promise.then(
        (res) => {
          this.router.navigate([`./project/${res.name}`]);
        },
        (error) => {
          this.alerts.addError("New project error", error);
        });
  }
} 