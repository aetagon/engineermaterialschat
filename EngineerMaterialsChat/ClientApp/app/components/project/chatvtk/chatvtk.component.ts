import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Util } from '../../../constants/app.utils';

@Component({
  selector: 'chat-vtk',
  templateUrl: './chatvtk.component.html',
  styleUrls: ['./chatvtk.component.css'],
})
export class ChatVtkComponent {
  fileNameUrl: SafeUrl;
  urlBase: string = "./plugins/vtk.js/GeometryViewer/index.html";
  visible: boolean = true;

  constructor(private sanitizer: DomSanitizer) {
    this.setFilename('');
  }

  setFilename(url: string) {
    let safeUrl;
    if (!Util.stringNullOrEmpty(url))
      safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`${this.urlBase}?fileURL=${url}`);
    else
      safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`${this.urlBase}`);
    this.fileNameUrl = safeUrl;
  }

}

