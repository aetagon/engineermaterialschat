import { Component, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { ServerMessage } from '../../../classes/ServerMessage';
import { SignalRService } from '../../../services/signalr.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { OnInit } from '@angular/core';
import { CustomAlertComponent } from '../../customalert/customalert.component';
import { Util } from '../../../constants/app.utils';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'ishi-chat',
  templateUrl: './ishichat.component.html',
  styleUrls: ['./ishichat.component.css']
})
export class IshiChatComponent {
  @Output() fileButtonClicked: EventEmitter<string> = new EventEmitter();

  constructor(private signalr: SignalRService,
              private activatedRoute: ActivatedRoute,
              private alert: AlertService)
  {
    this.subscribeToEvents();
  }

  @ViewChild('chatBox') myScrollContainer: ElementRef;

  projectId = null;
  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.projectId = params['id'];
    });
  }

  chatMessage: string;
  messageList: Array<ClientMessage> = []

  addMessage() {
    let chatText = this.chatMessage.trim();
    if (chatText.length > 0) {
      this.chatMessage = "";

      let msgToSend: ServerMessage = {
        Sender: "Alexander Pierce",
        Text: chatText,
        Timestamp: new Date(),
        ChatRoomName: `Project_${this.projectId}`,
        AttachmentUrl: ""
      };
      this.signalr.sendChatMessage(msgToSend);
    }
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  formatDate(dateText: string) {
    let date = new Date(dateText);
    return `${date.getFullYear()}/${this.pad(date.getMonth() + 1, 2)}/${this.pad(date.getDate(), 2)} 
            ${this.pad(date.getHours(), 2)}:${this.pad(date.getMinutes(), 2)}:${this.pad(date.getSeconds(), 2)}`;
  }

  pad(num: number, size: number) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }

  private subscribeToEvents(): void {
    // if connection exists it can call of method.  
    this.signalr.connectionEstablished.subscribe(() => {
      console.log("Chat connected");
    });
    // finally our service method to call when response received from server event and transfer response to some variable to be shwon on the browser.  
    this.signalr.messageReceived.subscribe((message: ServerMessage) => {
      //console.log(message.Text);
      this.messageList.push({
        messageText: message.Text,
        sender: message.Sender,
        senderPicUrl: (message.Sender != "Alexander Pierce") ? "/img/default-user-image.png" : "/img/user2-160x160.jpg",
        ofCurrentUser: message.Sender == "Alexander Pierce",
        timestamp: message.Timestamp,
        attachment: getAttachment(message)
      });

      this.messageList = this.messageList.sort((m1, m2) => (m1.timestamp > m2.timestamp) ? 1 : -1);

      //this.alert.addSuccess("Success", "You are connected");
    });

    function getAttachment(message: ServerMessage) {
      if (Util.stringNullOrEmpty(message.AttachmentUrl))
        return null;

      return {
        url: message.AttachmentUrl,
        name: getFilename(message.AttachmentUrl),
        isImage: isImage(message.AttachmentUrl)
      };
    }

    function isImage(url: string) {
      let extensions = ['JPG', 'PNG', 'BMP'];

      let urlParts = url.split(".");
      let extension = urlParts.slice(-1)[0];
      return extensions.indexOf(extension.toUpperCase()) > -1;
    }

    function getFilename(url: string) {
      let urlPars = url.split("/");
      let index = urlPars.length - 1;
      return urlPars[index];
    }
  }

  openVtpFile(url: string) {
    this.fileButtonClicked.emit(url);
  }

  openConnection() {

  }
}

interface ClientMessage {
  messageText: string;
  sender: string;
  senderPicUrl: string;
  ofCurrentUser: boolean,
  timestamp: Date;
  attachment: any;
}

class Attachment {
  url: string;
  name: string;
  isImage: boolean;
}

