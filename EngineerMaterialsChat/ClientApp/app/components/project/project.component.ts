﻿import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '../../services/alert.service'
import { ProjectsService } from '../../services/projects.service'
import { Project } from '../../classes/Project';
import { AppUser } from '../../services/userManagement.service';

@Component({
    selector: 'project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css'],
})
export class ProjectComponent {
    project: Project = {
        id: '',
        name: '',
        creator: '',
        creatorName: '',
        description: '',
        creation: '',
        role: ''
    }
    showDetails: boolean = false;
    shareMail: string;
    profileId: string;
    projectUsers: Array<SharedUser> = [];

    constructor(private alerts: AlertService, public projectFunc: ProjectsService, private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.loadProject();
        this.projectFunc.loadProfiles();
        this.loadFiles();
    }

    private loadProject() {
        let id = this.route.snapshot.paramMap.get('id');
        let promise = this.projectFunc.find(id);
        if (promise)
            promise.then(
                (resp) => {
                    this.project = Object.assign({}, resp);
                    this.getSharedUsers();
                },
                (error) => {
                    this.alerts.addError("Project error", error.message);
                });
    }

    private loadFiles() {
        let id = this.route.snapshot.paramMap.get('id');
        let promise = this.projectFunc.loadFiles(id);
        if (promise)
            promise.then(
                (resp) => {

                },
                (error) => {
                    this.alerts.addError("Project files error", error.message);
                });
    }

    openVtkFile(url: string) {
        let event = url;
    }

    setNewProjectName(newName: string) {
        this.project.name = newName;
    }

    setNewProjectDesc(newDesc: string) {
        this.project.description = newDesc;
    }

    updateProject() {
        let id = this.route.snapshot.paramMap.get('id');
        if (!id)
            return;
        let project: Project = {
            id: id,
            creation: this.project.creation,
            creator: this.project.creator,
            description: this.project.description,
            name: this.project.name,
            role: '',
            creatorName: ''
        };

        let promise = this.projectFunc.update(project);
        if (promise)
            promise.then(
                (res) => {
                    this.loadProject()
                },
                (error) => { });
    }

    expandDetails() {
        this.showDetails = true;
    }

    squeezeDetails() {
        this.showDetails = false;
    }

    shareProject() {
        let email = this.shareMail;
        let profileId = this.profileId;
        let projectId = this.route.snapshot.paramMap.get('id');
        if (!email || !profileId || !projectId)
            return;

        this.projectFunc.addUser(email, profileId, projectId).then(
            (resp: any) => {
                this.getSharedUsers();
            },
            (error: any) => {
                for (let err of error.errors)
                    this.alerts.addError("Projects user error", err.message);
            });
    }

    getSharedUsers() {
        let id = this.route.snapshot.paramMap.get('id');
        if (!id)
            return;
        let promise = this.projectFunc.findSharedUsers(id);
        if (promise)
            promise.then(
                resp => {
                    this.projectUsers = [];
                    for (let user of resp.users)
                        this.projectUsers.push(user);
                },
                error => {
                    this.alerts.addError("Shared users error", error.message);
                });
    }

    uploadFile() {
        let x: any = document.getElementById("fileUpload");
        if (!x)
            return;

        let me = this;
        var txt = "";
        if ('files' in x) {
            if (x.files.length == 0) {
                txt = "Select a file to upload";
            } else {
                var file = x.files[0];

                if (file.size < 1024 * 1024) {
                    var reader = new FileReader();
                    reader.onload = function(evt: any) {
                        let content = evt.target.result;
                        let id = me.route.snapshot.paramMap.get('id');
                        if (id) {
                            let promise = me.projectFunc.addFile(id, file.name, content);
                            if (promise)
                                promise.then(
                                    resp => {
                                        me.loadFiles();
                                    },
                                    error => { });
                        }
                        x.value == "";
                    }
                    reader.onerror = function(evt) {
                        txt = "error reading file";
                    }
                    reader.readAsText(file, "UTF-8");
                }
                else {
                    txt = "Maximum size of file 1MB";
                }
            }
        }
        else {
            if (x.value == "") {
                txt += "Select a file to upload";
            } else {
                txt += "The files property is not supported by your browser!";
                txt += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
            }
        }
        let error: any = document.getElementById("fileError");
        if (error)
            error.innerHTML = txt;
        x.value = null;
    }

    download(filename: string, text: string) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

}

class SharedUser {
    name: string;
    email: string;
    role: string;
}
