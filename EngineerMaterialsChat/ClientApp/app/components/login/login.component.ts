import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserManagementService } from '../../services/userManagement.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  email: string;
  password: string;

  constructor(private user: UserManagementService, private alert: AlertService, private router: Router) {

  }

  signIn() {
    this.user.signIn(this.email, this.password)
      .then(
      resp => {
        this.router.navigate(['/']);
      },
      error => {
        for (let err of error.errors)
          this.alert.addError("Login Error", err.message);
      });
  }
}