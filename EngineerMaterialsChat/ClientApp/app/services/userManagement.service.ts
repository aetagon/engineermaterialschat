﻿import { EventEmitter, Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { CustomHttpService } from './customHttp.service';
import { AlertService } from '../services/alert.service';
import { Util } from '../constants/app.utils';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Injectable()
export class UserManagementService {
  private currentUser: AppUser = new AppUser(this.sanitizer);
  private fetchingUser: boolean = false;
  private isBrowser: boolean = false;

  constructor(
    @Inject(PLATFORM_ID) platformId: string,
    private http: CustomHttpService,
    private alerts: AlertService,
    private sanitizer: DomSanitizer) {
    let testBrowser = isPlatformBrowser(platformId);
    if (testBrowser) {
      this.isBrowser = true;
    }
  }

  isLoggedIn(): boolean {
    let tokens = Util.getUserTokens();
    return tokens.localId != null;
    //return true;
  }

  register(email: string, invitationMessage: string) {
    let data = {
      email: email,
      invitationMessage: invitationMessage
    };

    return this.http.post("home/register", data, "")
  }

  signIn(email: string, password: string) {
    let data = {
      email: email,
      password: password
    };

    var res = this.http.post("home/signin", data, "");
    res.then(
      resp => {
        Util.setUserTokens(resp);
        this.getUserInfo();
      },
      error => {
        this.alerts.addError("User error", error.message);
      });

    return res;
  }

  signOut() {
    this.currentUser.reset();
    Util.deleteUserTokens();
  }

  public getUserInfo() {
    let tokens = Util.getUserTokens();
    if (!tokens.localId)
      return;

    this.fetchingUser = true;
    let url = `home/getuser/${tokens.localId}`;

    var res = this.http.get(url, tokens.idToken);
    res.then(
      resp => {
        this.currentUser.copyFom(resp);

        if (this.currentUser.isEmpty())
          this.signOut();
        this.fetchingUser = false;
      },
      error => {
        this.alerts.addError("User error", error.message);
        this.signOut();
        this.fetchingUser = false;
      });

    return res;
  }

  private needsFetch() {
    return this.isBrowser && this.currentUser.isEmpty() && !this.fetchingUser;
  }

  getCurrentUser(): AppUser {
    if (this.needsFetch())
      this.getUserInfo();
    return this.currentUser;
  }

  update(user: AppUser): Promise<any> {
    const tokens = Util.getUserTokens() || { idToken: "" };
    return this.http.put("home/UpdateUser", user, tokens.idToken);
  }

}

export class AppUser {
  private readonly defaultProfilePic: SafeResourceUrl;// = this.sanitizer.bypassSecurityTrustResourceUrl('/img/default-user-image.png');
  private readonly defaultPagePic: SafeResourceUrl;// = this.sanitizer.bypassSecurityTrustResourceUrl('/img/placeholder-image.jpg');
  id: string = '';
  email: string = '';
  surname: string = '';
  name: string = '';
  occupation: string = '';
  profilePicUrl: string = '';
  pagePicUrl: string = '';
  registered: Date | null = null;

  constructor(private sanitizer: DomSanitizer | null) {
    if (sanitizer) {
      this.defaultProfilePic = sanitizer.bypassSecurityTrustResourceUrl('/img/default-user-image.png');
      this.defaultPagePic = sanitizer.bypassSecurityTrustResourceUrl('/img/placeholder-image.jpg');
    }
  }

  reset(): void {
    this.id = '';
    this.email = '';
    this.surname = '';
    this.name = '';
    this.occupation = '';
    this.profilePicUrl = '';
    this.pagePicUrl = '';
    this.registered = null;
  }

  copyFom(from: any): void {
    this.id = from.id || '';
    this.email = from.email || '';
    this.surname = from.surname || '';
    this.name = from.name || '';
    this.occupation = from.occupation || '';
    this.profilePicUrl = from.profilePicUrl || '';
    this.pagePicUrl = from.pagePicUrl || '';
    this.registered = new Date(from.registered) || null;
  }

  isEmpty(): boolean {
    return Util.stringNullOrEmpty(this.id);
  }

  safeProfilePicUrl() {
    return (Util.stringNullOrEmpty(this.profilePicUrl) || !this.sanitizer)
      ? this.defaultProfilePic
      : this.sanitizer.bypassSecurityTrustResourceUrl(this.profilePicUrl);
  }

  safePagePicUrl() {
    return (Util.stringNullOrEmpty(this.pagePicUrl) || !this.sanitizer)
      ? this.defaultPagePic
      : this.sanitizer.bypassSecurityTrustResourceUrl(this.pagePicUrl);
  }
}