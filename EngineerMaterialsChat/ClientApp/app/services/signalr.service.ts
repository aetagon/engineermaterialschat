﻿import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection } from '@aspnet/signalr-client';
import { Observable } from 'rxjs/Observable';

import { ServerMessage } from '../classes/ServerMessage';
import { CONFIGURATION } from '../constants/app.constants';

@Injectable()
export class SignalRService {

  messageReceived: EventEmitter<ServerMessage>;
  connectionEstablished: EventEmitter<Boolean>;
  connectionExists = false;

  private _hubConnection: HubConnection;

  constructor() {
    this.messageReceived = new EventEmitter<ServerMessage>();
    this.connectionEstablished = new EventEmitter<Boolean>();

    this._hubConnection = new HubConnection(CONFIGURATION.baseUrls.server + 'Chat');

    this.registerOnServerEvents();

    this.startConnection();
  }

  public sendChatMessage(message: ServerMessage) {
    this._hubConnection.invoke('SendToClients', message);
  }

  private startConnection(): void {

    this._hubConnection.start()
      .then(() => {
        console.log('Hub connection started');
        this.connectionEstablished.emit(true);
      })
      .catch(err => {
        console.log('Error while establishing connection')
      });
  }

  private registerOnServerEvents(): void {

    this._hubConnection.on('MessageToClient', (message: ServerMessage) => {
      this.messageReceived.emit(message);
    });

    this._hubConnection.on('Send', (data: any) => {
      this.messageReceived.emit(data);
    });
  }
}