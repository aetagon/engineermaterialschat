﻿import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AlertService {

  @Output() alertdAdded: EventEmitter<Alert> = new EventEmitter();

  constructor() {

  }

  addInfo(title: string, message: string) {
    let alert = {
      Title: title,
      Message: message,
      Class: 'alert-info',
      IconClass: 'fa-info'
    }

    this.alertdAdded.emit(alert);
  }

  addWarning(title: string, message: string) {
    let alert = {
      Title: title,
      Message: message,
      Class: 'alert-warning',
      IconClass: 'fa-warning'
    }

    this.alertdAdded.emit(alert);
  }

  addSuccess(title: string, message: string) {
    let alert = {
      Title: title,
      Message: message,
      Class: 'alert-success',
      IconClass: 'fa-check'
    }

    this.alertdAdded.emit(alert);
  }

  addError(title: string, message: string) {
    let alert = {
      Title: title,
      Message: message,
      Class: 'alert-danger',
      IconClass: 'fa-ban'
    }

    this.alertdAdded.emit(alert);
  }

}

export interface Alert {
  Title: string;
  Message: string;
  Class: string;
  IconClass: string;
}