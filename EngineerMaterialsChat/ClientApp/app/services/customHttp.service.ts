﻿import { EventEmitter, Injectable, Output } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';

@Injectable()
export class CustomHttpService {
  private pendingRequests: number = 0;

  constructor(private $http: Http) {

  }

  get(url: string, idToken: string): Promise<any> {
    this.pendingRequests++;
    let me = this;

    let headers = new Headers();
    headers.append("auth", idToken);

    return new Promise((resolve, reject) => {
      this.$http.get(url, { headers: headers }).subscribe(handleSuccess, handleError);

      function handleSuccess(resp: any) {
        let respObj = JSON.parse(resp._body);

        if (respObj.fullError) {
          handleError(respObj.fullError);
          return;
        }
        resolve(respObj.payload);
        me.pendingRequests--;
      }

      function handleError(error: any) {
        reject(error);
        me.pendingRequests--;
      }
    });
  }

  post(url: string, request: any, idToken: string): Promise<any> {
    this.pendingRequests++;
    let me = this;

    let headers = new Headers();
    headers.append("auth", idToken);

    return new Promise((resolve, reject) => {
      this.$http.post(url, request, { headers: headers }).subscribe(handleSuccess, handleError);

      function handleSuccess(resp: any) {
        let respObj = JSON.parse(resp._body);

        if (respObj.fullError) {
          handleError(respObj.fullError);
          return;
        }
        resolve(respObj.payload);
        me.pendingRequests--;
      }

      function handleError(error: any) {
        reject(error);
        me.pendingRequests--;
      }
    });
  }

  put(url: string, request: any, idToken: string): Promise<any> {
    this.pendingRequests++;
    let me = this;

    let headers = new Headers();
    headers.append("auth", idToken);

    return new Promise((resolve, reject) => {
      this.$http.put(url, request, { headers: headers }).subscribe(handleSuccess, handleError);

      function handleSuccess(resp: any) {
        let respObj = JSON.parse(resp._body);

        if (respObj.fullError) {
          handleError(respObj.fullError);
          return;
        }
        resolve(respObj.payload);
        me.pendingRequests--;
      }

      function handleError(error: any) {
        reject(error);
        me.pendingRequests--;
      }
    });
  }

  hasPendingConnections() {
    return this.pendingRequests > 0;
  }

}