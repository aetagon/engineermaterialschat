﻿import { EventEmitter, Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { CustomHttpService } from './customHttp.service';
import { AlertService } from '../services/alert.service';
import { Util } from '../constants/app.utils';
import { Project } from '../classes/Project';

@Injectable()
export class ProjectsService {
    private isBrowser: boolean = false;
    private projectTimeout: number = 60 * 1000 * 60 * 60;
    private projectsResp: any;

    public projects: Array<Project>;
    public profiles: Array<Profile>;
    public files: Array<File>;

    constructor(
        @Inject(PLATFORM_ID) platformId: string,
        private http: CustomHttpService,
        private alerts: AlertService) {
        this.isBrowser = isPlatformBrowser(platformId);
        //if (this.isBrowser)
        //  this.loadProfiles();
    }

    add() {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        let project: Project = {
            id: '',
            name: 'new project',
            description: "project's description",
            creation: '',
            creator: idTokens.localId,
            creatorName: '',
            role: ''
        };
        let promise = this.http.post("home/addproject", project, idTokens.idToken);

        promise.then(resp => {
            this.clearCachedProjects();
        }, error => {
            this.clearCachedProjects();
            this.alerts.addError("Project error", error.message);
        })

        return promise;
    }

    addUser(email: string, profile: string, projectId: string) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return new Promise(() => { });

        let projectUser = {
            projectId: projectId,
            userEmail: email,
            profileId: profile
        };
        return this.http.post("home/addprojectuser", projectUser, idTokens.idToken);
    }

    update(project: Project) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        let promise = this.http.put("home/updateproject", project, idTokens.idToken);
        promise.then(resp => {
            this.alerts.addSuccess("Project update success", "Project was successfuly updated");
            this.clearCachedProjects();
        }, error => {
            this.alerts.addError("Project update error", error);
            this.clearCachedProjects();
        })

        return promise;
    }

    find(projectId: string | null) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        return this.http.get(`home/getproject/${projectId}`, idTokens.idToken)
    }

    findAll() {
        if (this.projectsResp)
            return new Promise((resolve, reject) => {
                resolve(this.projectsResp);
            });

        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        let promise = this.http.get(`home/getprojects/${idTokens.localId}`, idTokens.idToken);
        promise.then(
            resp => {
                this.projectsResp = resp;
                this.projects = resp.projects.map((x: any) => new Project(x));
                setTimeout(this.clearCachedProjects, this.projectTimeout);
            },
            error => {
                this.alerts.addError("Get Projects error", error.message);
            });

        return promise;
    }

    findSharedUsers(projectId: string) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        return this.http.get(`home/getsharedusers/${projectId}`, idTokens.idToken);
    }

    getProjectsCount(): number {
        if (this.projects)
            return this.projects.length;
        else
            return 0;
    }

    loadProfiles() {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        this.http.get("home/getprofiles", idTokens.idToken).then(
            resp => {
                this.profiles = [];
                for (let profile of resp.profiles)
                    this.profiles.push(profile);
            },
            error => {
                this.alerts.addError("Profiles error", error.message);
            });
    }

    loadFiles(projectId: string | null) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        let promise = this.http.get(`home/GetFiles/${projectId}`, idTokens.idToken);
        promise.then(
            resp => {
                this.files = resp.files.map((x: File) => Object.assign({}, x));
            },
            error => {
                this.alerts.addError("Get Projects error", error.message);
            });

        return promise;
    }

    addFile(projectId: string, fname: string, content: string) {
        let idTokens = Util.getUserTokens();
        if (!idTokens.localId)
            return null;

        let req: File = {
            projectId: projectId,
            fileName: fname,
            fileContent: content,
            dateCreated: '',
            dateUpdated: ''
        }

        let promise = this.http.post(`home/AddFile`, req, idTokens.idToken);
        promise.then(
            resp => {
                this.alerts.addSuccess("Add file success", "Your file was successfuly uploaded");
            },
            error => {
                this.alerts.addError("Upload File error", error.message);
            });

        return promise;
    }

    private clearCachedProjects() {
        this.projectsResp = null;
    }
}

class Profile {
    id: string;
    name: string;
}

class File {
    projectId: string;
    fileName: string;
    fileContent: string;
    dateCreated: string;
    dateUpdated: string;
}