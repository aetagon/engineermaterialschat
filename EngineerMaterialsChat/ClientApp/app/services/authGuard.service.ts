﻿// auth.guard.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserManagementService } from './userManagement.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private user: UserManagementService, private router: Router) { }

  canActivate() {

    if (!this.user.isLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }
}