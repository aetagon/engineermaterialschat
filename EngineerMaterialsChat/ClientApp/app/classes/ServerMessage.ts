﻿export class ServerMessage {
  public Text: string;
  public Sender: string;
  public Timestamp: Date;
  public ChatRoomName: string;
  public AttachmentUrl: string;
}