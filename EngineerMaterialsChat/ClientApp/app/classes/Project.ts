﻿export class Project {
  constructor(copyFrom: any) {
    this.id = copyFrom.id;
    this.name = copyFrom.name;
    this.creator = copyFrom.creator;
    this.creatorName = copyFrom.creatorName;
    this.description = copyFrom.description;
    this.creation = copyFrom.creation;
    this.role = copyFrom.role;
  }
  id: string;
  name: string;
  creator: string;
  creatorName: string;
  description: string;
  creation: string;
  role: string;
}