﻿interface IDictionaryNumberKey {
  [key: number]: string
}

interface IDictionaryStringKey {
  [key: string]: string
}