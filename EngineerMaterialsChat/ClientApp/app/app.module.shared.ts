import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProjectComponent } from './components/project/project.component';
import { CustomModalComponent } from './components/custommodal/custommodal.component';
import { ChatVtkComponent } from './components/project/chatvtk/chatvtk.component';
import { IshiChatComponent } from './components/project/ishichat/ishichat.component';
import { SignalRService } from './services/signalr.service';
import { CustomAlertComponent } from './components/customalert/customalert.component';
import { AlertService } from './services/alert.service';
import { CustomHttpService } from './services/customHttp.service';
import { AuthGuardService } from './services/authGuard.service';
import { UserManagementService } from './services/userManagement.service';
import { ProjectsService } from './services/projects.service';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    ProjectComponent,
    CustomModalComponent,
    ChatVtkComponent,
    IshiChatComponent,
    CustomAlertComponent,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
      //{ path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
      { path: 'project/:id', component: ProjectComponent, canActivate: [AuthGuardService] },
      { path: '**', redirectTo: 'home' }
    ])
  ],
  providers: [SignalRService, CustomAlertComponent, AlertService, CustomHttpService, AuthGuardService, UserManagementService, ProjectsService]
})
export class AppModuleShared {
}
