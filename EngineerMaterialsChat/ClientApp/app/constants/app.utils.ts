﻿export class Util {
  public static tokenName: string = "user_tokens";

  static stringNullOrEmpty(text: string | null): boolean {
    if (text == null)
      return true;

    let trimmed = text.trim();
    if (trimmed.length === 0)
      return true;

    return false;
  }

  static getUserTokens(): AppUserTokens {
    let userText = localStorage.getItem(Util.tokenName)
    return (userText) ? JSON.parse(userText) : {};
  }

  static setUserTokens(tokens: AppUserTokens): void {
    let userText = JSON.stringify(tokens);
    localStorage.setItem(Util.tokenName, userText);
  }

  static deleteUserTokens(): void {
    localStorage.removeItem(Util.tokenName);
  }
}

export class AppUserTokens {
  kind: string;//string	The request type, always "identitytoolkit#VerifyPasswordResponse".
  idToken: string;//	string	A Firebase Auth ID token for the authenticated user.
  email: string;//	string	The email for the authenticated user.
  refreshToken: string;//string	A Firebase Auth refresh token for the authenticated user.
  expiresIn: string;//	string	The number of seconds in which the ID token expires.
  localId: string;//	string	The uid of the authenticated user.
  registered: boolean;//	boolean	Whether the email is for an existing account.
}