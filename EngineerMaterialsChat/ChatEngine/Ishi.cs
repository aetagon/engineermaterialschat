﻿using EngineerMaterials.Chat;
using EngineerMaterials.Chat.Constants;
using System;
using System.Linq;

namespace EngineerMaterials.Chat.Engine
{
    class Ishi
    {
        public delegate void MessageCreated(Message newMessage);
        public event MessageCreated OnNewMessageCreated;

        private Uri baseUrl = new Uri(Connections.DocumentsBaseUrl);

        public void Input(string chatRoom, params string[] args)
        {
            if (args.Contains("image", StringComparer.OrdinalIgnoreCase))
            {
                OnNewMessageCreated(new Message
                {
                    Sender = "Ishi",
                    Text = "Click on the thumb to see the image",
                    Timestamp = DateTime.Now,
                    ChatRoomName = chatRoom,
                    AttachmentUrl = new Uri(baseUrl, "img/photo2.png").AbsoluteUri
                });
                return;
            }

            if (args.Contains("file", StringComparer.OrdinalIgnoreCase))
            {
                OnNewMessageCreated(new Message
                {
                    Sender = "Ishi",
                    Text = "Click on the open button to open the file or on the arrow button to download it",
                    Timestamp = DateTime.Now,
                    ChatRoomName = chatRoom,
                    AttachmentUrl = new Uri(baseUrl, "files/diskout.vtp").AbsoluteUri
                });
                return;
            }

            OnNewMessageCreated(new Message
            {
                Sender = "Ishi",
                Text = args.Length.ToString(),
                Timestamp = DateTime.Now,
                ChatRoomName = chatRoom
            });

            OnNewMessageCreated(new Message
            {
                Sender = "Ishi",
                Text = "Thank you very much",
                Timestamp = DateTime.Now,
                ChatRoomName = chatRoom
            });
        }
    }
}