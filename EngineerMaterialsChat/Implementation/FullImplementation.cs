﻿using EngineerMaterials.Chat.Constants;
using EngineerMaterials.Chat.Controllers;
using EngineerMaterials.Firebase;
using EngineerMaterials.Firebase.Types;
using EngineerMaterials.Mailgun;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EngineerMaterials.Chat.Implementation
{
    public class FullImplementation
    {
        private readonly IConfiguration _config;
        private readonly UserManagement _fbManager;
        private readonly DbManagement _dbManager;
        private Dictionary<string, string> _profiles;
        ModelStateDictionary _modelState;
        HttpContext _httpContext;

        public FullImplementation(IConfiguration config, HttpContext httpContext, ModelStateDictionary modelState)
        {
            _config = config;
            _modelState = modelState;
            _httpContext = httpContext;
            _fbManager = new UserManagement(_config["Firebase:ApiKey"]);
            _dbManager = new DbManagement(_config["Firebase:ProjectId"]);
        }

        public FirebaseUserResponse Register(RegisterModel Input)
        {
            if (!_modelState.IsValid)
            {
                var errors = _modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
                return createError<FirebaseUserResponse>(errors);
            }

            var password = Input.password;
            if (string.IsNullOrWhiteSpace(password))
                password = Security.GenerateRandomPassword(new PasswordOptions
                {
                    RequireDigit = true,
                    RequiredLength = 20,
                    RequireLowercase = true,
                    RequireUppercase = true
                });

            var res = _fbManager.Register(Input.email, password).Result;
            if (res.error == null)
            {
                var url = Connections.DocumentsBaseUrl + "login";
                var mailFunc = new Mail(_config["Mailgun:Domain"], _config["Mailgun:ApiKey"]);
                var mail = new MailDetails
                {
                    Subject = "Engineer Materials Invitation",
                    From = _config["Mailgun:From"],
                    To = new string[] { Input.email },
                    Html = composeMessage(Input.email, password, url, Input.invitationMessage)
                };
                var mailRes = mailFunc.Send(mail);
                if (mailRes.StatusCode != System.Net.HttpStatusCode.OK)
                    return createError<FirebaseUserResponse>(new string[] { mailRes.Result.message });
            }

            return res;
        }

        public FirebaseUserResponse SignIn(LoginModel Input)
        {
            if (!_modelState.IsValid)
            {
                var errors = _modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
                return createError<FirebaseUserResponse>(errors);
            }

            return _fbManager.SignIn(Input.email, Input.password).Result;
        }

        public ApplicationUser GetUser(string id)
        {
            var idToken = _httpContext.Request.Headers["auth"];

            var res = getUserFromDb(id, idToken);
            if (res == null)
            {
                var email = getEmail(idToken);
                if (email == null)
                    return null;

                var newUser = new ApplicationUser
                {
                    id = id,
                    email = email
                };
                return addUserToDb(newUser, idToken);
            }

            return res;
        }

        public ApplicationUser UpdateUser(ApplicationUser user)
        {
            if (string.IsNullOrWhiteSpace(user.id))
                return createError<ApplicationUser>(new string[] { "No user id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "users", user.id };
            user.registered = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd hh:mm:ss.fff");
            return _dbManager.UpdateObject(objectPath, user, idToken)?.Result;
        }

        public AddObjectResponse AddProject(Project project)
        {
            var idToken = _httpContext.Request.Headers["auth"];
            project.creation = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd hh:mm:ss.fff");

            string[] objectPath = { "projects" };
            var projectResp = _dbManager.AddObject(objectPath, project, idToken).Result;
            if (projectResp?.fullError != null)
                return projectResp;

            objectPath = new string[] { "user2project" };
            var req = new User2Project
            {
                projectId = projectResp.name,
                userId = project.creator,
                profileId = "01"
            };
            var res = _dbManager.AddObject(objectPath, req, idToken).Result;
            if (res.fullError != null)
                return res;

            return projectResp;
        }

        public Project UpdateProject(Project project)
        {
            if (string.IsNullOrWhiteSpace(project.id))
                return createError<Project>(new string[] { "No project id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "projects", project.id };
            return _dbManager.UpdateObject(objectPath, project, idToken).Result;
        }

        public Project GetProject(string id)
        {
            return innerGetProject(id).Result;
        }

        public Projects GetProjects(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return createError<Projects>(new string[] { "No user id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "user2project" };
            string[] propPath = { "userId" };
            List<User2Project> u2ps = getUser2Projet(id, idToken, objectPath, propPath).Result;

            var tasks = new List<Tuple<User2Project, Task<Project>>>();
            foreach (var u2p in u2ps)
                tasks.Add(new Tuple<User2Project, Task<Project>>(u2p, innerGetProject(u2p.projectId)));
            Task.WhenAll(tasks.Select(x => x.Item2).ToArray());

            return new Projects
            {
                projects = tasks.Select(x =>
                {
                    var project = x.Item2.Result;
                    var owner = GetUser(project.creator);
                    var ownerName = $"{owner.name} {owner.surname}";
                    project.id = x.Item1.projectId;
                    project.role = getProfiles(idToken)[x.Item1.profileId];
                    project.creatorName = ownerName;
                    return project;
                })
                .ToList()
            };
        }

        public SharedUsers GetSharedUsers(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return createError<SharedUsers>(new string[] { "No project id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "user2project" };
            string[] propPath = { "projectId" };
            List<User2Project> u2ps = getUser2Projet(id, idToken, objectPath, propPath).Result;

            return new SharedUsers
            {
                users = u2ps.Select(x =>
                {
                    var user = this.GetUser(x.userId);
                    return new SharedUser
                    {
                        name = $"{user.name} {user.surname}",
                        email = user.email,
                        role = getProfiles(idToken)[x.profileId]
                    };
                })
                .ToList()
            };
        }

        public AddObjectResponse AddProjectUser(User2ProjectRequest u2p)
        {
            var idToken = _httpContext.Request.Headers["auth"];
            var userId = getIdFromMail(u2p.userEmail, idToken);
            if (userId == null)
                return new AddObjectResponse
                {
                    error = "No user was found with provided email",
                    fullError = new Firebase.Types.Error(new string[] { "No user was found with provided email" })
                };

            var objectPath = new string[] { "user2project" };
            var req = new User2Project
            {
                projectId = u2p.projectId,
                userId = userId,
                profileId = u2p.profileId
            };
            return _dbManager.AddObject(objectPath, req, idToken).Result;
        }

        public Profiles GetProfiles()
        {
            var idToken = _httpContext.Request.Headers["auth"];

            return new Profiles
            {
                profiles = getProfiles(idToken)
                    .Where(x => x.Key != "01")
                    .Select(x => new Profile
                    {
                        id = x.Key,
                        name = x.Value
                    }).ToList()
            };
        }

        public DbResponse GetFiles(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return createError<Projects>(new string[] { "No project id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "files" };
            string[] propPath = { "projectId" };
            List<FireBaseFile> files = getFiles(id, idToken, objectPath, propPath).Result;

            return new FireBaseFiles
            {
                files = files
            };
        }

        public DbResponse AddFile(FireBaseFile file)
        {
            var res = new DbResponse();
            var idToken = _httpContext.Request.Headers["auth"];
            file.dateCreated = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss.fff");

            string[] objectPath = { "files" };
            var fileResp = _dbManager.AddObject(objectPath, file, idToken).Result;
            if (fileResp.fullError != null)
                res.fullError = fileResp.fullError;
            return res;
        }

        #region private methods

        private async Task<List<User2Project>> getUser2Projet(string id, Microsoft.Extensions.Primitives.StringValues idToken, string[] objectPath, string[] propPath)
        {
            var equalProp = new Tuple<string[], string>(propPath, id);
            var u2ps = await _dbManager.GetObjects<User2Project>(objectPath, equalProp, idToken);
            return u2ps;
        }

        private async Task<List<FireBaseFile>> getFiles(string id, Microsoft.Extensions.Primitives.StringValues idToken, string[] objectPath, string[] propPath)
        {
            var equalProp = new Tuple<string[], string>(propPath, id);
            var files = await _dbManager.GetObjects<FireBaseFile>(objectPath, equalProp, idToken);
            return files;
        }

        private static T createError<T>(string[] messages) where T : class
        {
            return new FirebaseUserResponse
            {
                error = new Firebase.Types.Error(messages)
            } as T;
        }

        private static string composeMessage(string mail, string password, string url, string message)
        {
            return $"<h1>Engineer Materials Invitation</h1>" +
                $"<p>You have been invited to join the engineermaterials application. " +
                $"Use the credentials username:{mail}, password:{password} to login here <a href='{url}'>{url}</a></p>" +
                $"<p><strong>Invitation message: </string> {message}</p>";
        }

        private ApplicationUser getUserFromDb(string id, string idToken)
        {
            string[] objectPath = { "users", id };
            var res = _dbManager.GetObject<ApplicationUser>(objectPath, idToken);

            return res.Result;
        }

        private ApplicationUser addUserToDb(ApplicationUser user, string idToken)
        {
            string[] objectPath = { "users", user.id };
            var res = _dbManager.UpdateObject(objectPath, user, idToken);

            return res.Result;
        }

        private string getEmail(string idToken)
        {
            var res = _fbManager.GetMail(idToken);
            var taskRes = res.Result;
            var user = taskRes.users.FirstOrDefault();
            return user?.email;
        }

        private string getIdFromMail(string email, string idToken)
        {
            string[] objectPath = { "users" };
            var equalTo = new Tuple<string[], string>(new string[] { "email" }, email);
            var res = _dbManager.GetObjects<ApplicationUser>(objectPath, equalTo, idToken);
            var taskRes = res.Result;
            var user = taskRes.FirstOrDefault();
            return user?.id;
        }

        private List<Profile> getProfilesFromDb(string idToken)
        {
            string[] objectPath = { "profiles" };
            var res = _dbManager.GetObject<List<Profile>>(objectPath, idToken);

            return res.Result;
        }

        private Dictionary<string, string> getProfiles(string idToken)
        {
            if (_profiles == null)
            {
                _profiles = new Dictionary<string, string>();
                var objectPath = new string[] { "profiles" };
                var task = _dbManager.GetObjects<Profile>(objectPath, null, idToken);
                foreach (var profile in task.Result)
                    _profiles.Add(profile.id, profile.name);
            }

            return _profiles;
        }

        private async Task<Project> innerGetProject(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return createError<Project>(new string[] { "No project id" });
            var idToken = _httpContext.Request.Headers["auth"];
            string[] objectPath = { "projects", id };
            return await _dbManager.GetObject<Project>(objectPath, idToken);
        }

        #endregion
    }
}
