﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EngineerMaterials.Mailgun
{
    public class Mail
    {
        private string _domainName;
        private string _apiKey;

        public Mail(string domainName, string apiKey)
        {
            _domainName = domainName;
            _apiKey = apiKey;
        }

        public MailResponse Send(MailDetails msgDetails)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", _apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _domainName, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", msgDetails.From);
            foreach (var recipient in msgDetails.To)
                request.AddParameter("to", recipient);
            foreach (var cc in msgDetails.CC)
                request.AddParameter("cc", cc);
            foreach (var bc in msgDetails.BC)
                request.AddParameter("bcc", bc);
            request.AddParameter("subject", msgDetails.Subject);
            request.AddParameter("text", msgDetails.Text);
            request.AddParameter("html", msgDetails.Html);

            request.Method = Method.POST;
            var res = client.Execute(request);
            return new MailResponse
            {
                Result = Newtonsoft.Json.JsonConvert.DeserializeObject<MailResult>(res.Content),
                StatusCode = res.StatusCode
            };
        }

    }

    public class MailDetails
    {
        public string Subject { get; set; }
        public string From { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        private string[] to;
        public string[] To
        {
            get
            {
                if (to == null)
                    return new string[0];
                return to;
            }
            set
            {
                to = value;
            }
        }
        private string[] cc;
        public string[] CC
        {
            get
            {
                if (cc == null)
                    return new string[0];
                return cc;
            }
            set
            {
                cc = value;
            }
        }
        private string[] bc;
        public string[] BC
        {
            get
            {
                if (bc == null)
                    return new string[0];
                return bc;
            }
            set
            {
                bc = value;
            }
        }
    }

    public class MailResponse
    {
        public MailResult Result { get; set; }
        public System.Net.HttpStatusCode StatusCode { get; set; }
    }

    public class MailResult
    {
        public string message { get; set; }
    }
}
